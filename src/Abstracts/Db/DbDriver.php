<?php

namespace Wtf\Abstracts\Db;

abstract class DbDriver {
    protected static $link = null;

    abstract public static function connect();

    abstract public static function query(string $sql);

    abstract public static function fetchRows(string $sql): array;

    abstract public static function fetchRow(string $sql): ?array;

    abstract public static function fetchValue(string $sql);

    abstract public static function insertIgnoreMany(string $table, array $rows);

    abstract public static function insertMany(string $table, array $rows, bool $ignore = false);

    abstract public static function insertOnDuplicateUpdate(string $table, array $rows);

    abstract public static function escape(string $value): string;

    /**
     * Alias of beginTransaction()
     */
    abstract public static function startTransaction();

    abstract public static function beginTransaction();

    abstract public static function beginTransactionReadOnly();

    abstract public static function beginTransactionReadWrite();

    abstract public static function beginTransactionWithConsistentSnapshot();

    abstract public static function commit();

    abstract public static function rollback();

    abstract public static function setAutoCommit(bool $mode);
}
