<?php

namespace Wtf\Helpers;

use Wtf\Wtf;

class Env {
    public static $filepath = null;
    protected static $cachedValues = null;

    public static function get(string $key, $default = null) {
        self::cacheValues();
        if (!isset(self::$cachedValues[$key])) {
            return $default;
        }

        return self::$cachedValues[$key];
    }

    protected static function cacheValues() {
        if (self::$cachedValues !== null) {
            return;
        }

        if (self::$filepath === null) {
            self::$filepath = Wtf::ENV_FILE_PATH;
        }
        $config = new Config(self::$filepath);
        if (!$config->exists()) {
            self::$cachedValues = [];
            return;
        }

        self::$cachedValues = $config->readValues();
    }
}
