<?php

namespace Wtf\Helpers;

class SpeedRacer {
    protected $chunkSize;
    protected $changeStep;
    protected $previousSpeed = 0;
    protected $previousSpeedChange = '';
    protected $tryRandomIn = 5;

    protected $timeStart;

    public function __construct(int $initialChunkSize, int $changeStep = null, int $tryRandomIn = 5) {
        $this->chunkSize = $initialChunkSize;
        if ($changeStep === null) {
            $changeStep = ceil($initialChunkSize / 20);
            if ($changeStep < 1) {
                $changeStep = 0;
            }
        }

        $this->changeStep = $changeStep;
        $this->tryRandomIn = $tryRandomIn;
    }

    public function startTimer() {
        $this->timeStart = microtime(true);
    }

    public function getTimeElapsed(): float {
        return microtime(true) - $this->timeStart;
    }

    public function getNewChunkSize(float $speed): int {
        if ($this->previousSpeed === 0) {
            $this->previousSpeed = $speed;
        }

        if ($speed < $this->previousSpeed * 0.9 && $this->previousSpeedChange === 'up') {
                $this->speedDown();
//            echo 'Down'.PHP_EOL;
        } elseif ($speed < $this->previousSpeed * 0.9 && $this->previousSpeedChange === 'down') {
            $this->speedUp();
//            echo 'Up'.PHP_EOL;
        } elseif ($speed > $this->previousSpeed * 1.1 && $this->previousSpeedChange === 'up') {
            $this->speedUp();
//            echo 'Up'.PHP_EOL;
        } elseif ($speed > $this->previousSpeed * 0.9 && $this->previousSpeedChange === 'down') {
                $this->speedDown();
//            echo 'Down'.PHP_EOL;
        } elseif ($speed < $this->previousSpeed * 0.8 &&
            $this->previousSpeedChange === '' ||
            $this->tryRandomIn === 0
        ) {
            $rand = random_int(0, 1);
            if ($rand === 0) {
                $this->speedDown();
//                echo 'Down random'.PHP_EOL;
            } else {
                $this->speedUp();
//                echo 'Up random'.PHP_EOL;
            }

            $this->tryRandomIn = 5;
        } else {
            $this->previousSpeedChange = '';
        }

        --$this->tryRandomIn;
        $this->previousSpeed = $speed;
        if ($this->chunkSize < 10) {
            $this->chunkSize = 10;
        }

        return $this->chunkSize;
    }

    public function speedUp() {
        $this->chunkSize += $this->changeStep;
        $this->previousSpeedChange = 'up';
    }

    public function speedDown() {
        $this->chunkSize -= $this->changeStep;
        $this->previousSpeedChange = 'down';
    }
}
