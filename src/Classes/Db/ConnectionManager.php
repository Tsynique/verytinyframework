<?php

namespace Wtf\Classes\Db;

use InvalidArgumentException;
use MongoDB\Driver\Manager;
use Wtf\Helpers\Env;

/**
 * Class ConnectionManager
 * @package Wtf\Classes\Db
 */
class ConnectionManager {
    const DEFAULT_HOST_ENV_PARAM = 'MONGO_HOST';
    const DEFAULT_PORT_ENV_PARAM = 'MONGO_PORT';
    const DEFAULT_DATABASE_ENV_PARAM = 'MONGO_DATABASE';
    const DEFAULT_USERNAME_ENV_PARAM = 'MONGO_USERNAME';
    const DEFAULT_PASSWORD_ENV_PARAM = 'MONGO_PASSWORD';
    const DEFAULT_AUTHDB_ENV_PARAM = 'MONGO_AUTH_DATABASE';

    /**
     * @param string $connection
     * @return array
     */
    public static function getConnectionParameters(string $connection = ''): array {
        $envSuffix = '';
        if (!empty($connection)) {
            $envSuffix = '_'.strtoupper($connection);
        }

        $host = Env::get(self::DEFAULT_HOST_ENV_PARAM.$envSuffix);
        $port = (int)Env::get(self::DEFAULT_PORT_ENV_PARAM.$envSuffix, 27017);
        $database = Env::get(self::DEFAULT_DATABASE_ENV_PARAM.$envSuffix);
        $user = Env::get(self::DEFAULT_USERNAME_ENV_PARAM.$envSuffix);
        $password = Env::get(self::DEFAULT_PASSWORD_ENV_PARAM.$envSuffix);
        $authDatabase = Env::get(self::DEFAULT_AUTHDB_ENV_PARAM.$envSuffix);

        if (empty($host) || empty($database)) {
            throw new InvalidArgumentException('Connection "'.$connection.'" does not exist!');
        }

        return [
            'host' => $host,
            'port' => $port,
            'database' => $database,
            'user' => $user,
            'password' => $password,
            'authDatabase' => $authDatabase,
        ];
    }

    /**
     * @param array $params
     * @return string
     */
    private static function getConnectionUri(array $params): string {
        $credentials = '';
        if (!empty($params['user'])) {
            $credentials = sprintf('%s:%s@', $params['user'], $params['password']);
        }

        $connectUri = sprintf(
            'mongodb://%s%s:%d',
            $credentials,
            $params['host'],
            $params['port']
        );

        if (!empty($params['authDatabase'])) {
            $connectUri .= '/'.$params['authDatabase'];
        }
        return $connectUri;
    }

    /**
     * @param string $connection
     * @return MongoDbDriver
     */
    public static function connect(string $connection = ''): MongoDbDriver {
        $connectionParams = self::getConnectionParameters($connection);
        $connectUri = self::getConnectionUri($connectionParams);
        return new MongoDbDriver(
            new Manager($connectUri),
            $connectionParams['database']
        );
    }
}