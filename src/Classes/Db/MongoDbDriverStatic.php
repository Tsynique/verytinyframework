<?php

namespace Wtf\Classes\Db;

use Wtf\Helpers\Env;
use Exception;
use MongoDB\Collection;
use MongoDB\Database;

class MongoDbDriverStatic {
    protected static $link = null;
    protected static $database = null;

    public static function connect() {
        if (self::$link !== null) {
            return self::$link;
        }

        $host = Env::get('MONGO_HOST', 'localhost');
        self::$database = Env::get('MONGO_DATABASE');
        $user = Env::get('MONGO_USERNAME');
        $password = Env::get('MONGO_PASSWORD', '');
        $port = Env::get('MONGO_PORT', 27017);


        $connectUri = sprintf(
            'mongodb://%s:%d',
            $host,
            $port
        );

        try {
            self::$link = new \MongoDB\Driver\Manager($connectUri);
        } catch (Exception $e) {
            die($e->getMessage());
        }

        return self::$link;
    }

    public static function query(string $sql) {
        die('not implemented');
    }

    public static function fetchDocuments(
        string $collectionTitle,
        array $find = [],
        int $limit = null,
        array $sort = null,
        array $findOptions = []
    ): array {
        self::connect();
        if ($limit !== null) {
            $findOptions['limit'] = $limit;
        }
        if ($sort !== null) {
            $findOptions['sort'] = $sort;
        }

        $query = new \MongoDB\Driver\Query($find, $findOptions);

        $cursor = self::$link->executeQuery(self::$database.'.'.$collectionTitle, $query);
        return $cursor->toArray();
    }

    public static function fetchDocumentsCursor(
        string $collection,
        array $find = [],
        int $limit = null,
        array $options = []
   ): \MongoDB\Driver\Cursor {
        self::connect();
        if ($limit !== null) {
            $options['limit'] = $limit;
        }

        $query = new \MongoDB\Driver\Query($find, $options);
        return self::$link->executeQuery(self::$database.'.'.$collection, $query);
   }

    public static function fetchDocument(string $collection, array $find = [], array $sort = null): ?array {
        self::connect();
        $findOptions = [
            'limit' => 1,
        ];
        if ($sort !== null) {
            $findOptions['sort'] = $sort;
        }

        $query = new \MongoDB\Driver\Query($find, $findOptions);
        $cursor = self::$link->executeQuery(self::$database.'.'.$collection, $query);

        $rows = $cursor->toArray();
        if (isset($rows[0])) {
            return (array)$rows[0];
        }

        return null;
    }

    public static function fetchValue(string $collection, string $key, array $find = [], array $options = []) {
        self::connect();
        $options['limit'] = 1;
        $query = new \MongoDB\Driver\Query($find, $options);
        $cursor = self::$link->executeQuery(self::$database.'.'.$collection, $query);

        foreach ($cursor as $item) {
            return $item->{$key};
        }

        return null;
    }

    public static function insertIgnoreMany(string $collection, array $documents) {
        if (count($documents) === 0) {
            return;
        }

        self::connect();
        $bulk = new \MongoDB\Driver\BulkWrite();
        foreach ($documents as $document) {
            $document['_id'] = new \MongoDB\BSON\ObjectId();
            $bulk->insert($document);
        }

        try {
            self::$link->executeBulkWrite(self::$database.'.'.$collection, $bulk);
        } catch (\Exception $e) {

        }
    }

    public static function insertMany(string $collection, array $documents, bool $debugTime = false) {
        if (count($documents) === 0) {
            return;
        }

        $start = microtime(true);
        self::connect();
        $bulk = new \MongoDB\Driver\BulkWrite();
        foreach ($documents as $document) {
            $document['_id'] = new \MongoDB\BSON\ObjectId();
            $bulk->insert($document);
        }

//        try {
            self::$link->executeBulkWrite(self::$database.'.'.$collection, $bulk);
//        } catch (\Exception $e) {
//            die($e->getMessage());
//            echo 'Ignored'.PHP_EOL;
//            return;
//        }

        if ($debugTime) {
            $timeElapsed = microtime(true) - $start;
            printf(
                'Written %d document(s) in %0.4f s. (%0.4f docs/s.)'.PHP_EOL,
                count($documents),
                $timeElapsed,
                count($documents) / $timeElapsed
            );
        }
    }

    public static function updateOne(string $collectionTitle, array $filter, array $update, bool $upsert = false) {
        self::connect();
        $collection = new Collection(self::$link, self::$database, $collectionTitle);
        $options = [
            'upsert' => $upsert,
        ];
        $collection->updateOne($filter, $update, $options);
    }

    public static function delete(string $collectionTitle, array $find = []) {
        self::connect();
        $collection = new Collection(self::$link, self::$database, $collectionTitle);
        $collection->deleteMany($find);
    }

    public static function replaceOne(
        string $collectionTitle,
        array $filter,
        array $replacement,
        bool $upsert = true
    ) {
        self::connect();
        $collection = new Collection(self::$link, self::$database, $collectionTitle);
        $options = [
            'upsert' => $upsert,
        ];
        $collection->replaceOne($filter, $replacement, $options);
    }

    public static function insertOnDuplicateUpdate(string $table, array $rows) {
        die('insertOnDuplicateUpdate not implemented');
    }

    public static function escape(string $value): string {
        return addslashes($value);
    }

    public static function getDistinctValues(string $collectionTitle, string $key, array $filter = []): array {
        self::connect();
        $collection = new Collection(self::$link, self::$database, $collectionTitle);
        return (array)$collection->distinct($key, $filter);
    }

    public static function count(string $collectionTitle, array $filter = [], array $options = []): ?int {
       static::connect();

       $command = [
           'count' => $collectionTitle,
       ];

       $command['query'] = $filter;

       if (!empty($options['limit'])) {
           $command['limit'] = $options['limit'];
       }

       if (!empty($options['skip'])) {
           $command['skip'] = $options['skip'];
       }

       if (!empty($options['hint'])) {
           $command['hint'] = $options['hint'];
       }

       $commandResult = (new Database(self::$link, self::$database))->command($command)
           ->toArray();

       if (empty($commandResult) || empty($commandResult[0]->ok)) {
           return null;
       }

       return (int)$commandResult[0]->n;
   }

    public static function aggregate(string $collectionTitle, array $pipeline, array $options = []) {
        self::connect();
        $collection = new Collection(self::$link, self::$database, $collectionTitle);
        return $collection->aggregate($pipeline, $options);
    }

    /**
     * Alias of beginTransaction()
     */
    public static function startTransaction() {
        die('startTransaction not implemented');
    }

    public static function beginTransaction() {
        die('beginTransaction not implemented');
    }

    public static function beginTransactionReadOnly() {
        die('beginTransactionReadOnly not implemented');
    }

    public static function beginTransactionReadWrite() {
        die('beginTransactionReadWrite not implemented');
    }

    public static function beginTransactionWithConsistentSnapshot() {
        die('beginTransactionWithConsistentSnapshot not implemented');
    }

    public static function commit() {
        die('commit not implemented');
    }

    public static function rollback() {
        die('rollback not implemented');
    }

    public static function setAutoCommit(bool $mode) {
        die('setAutoCommit not implemented');
    }
}
