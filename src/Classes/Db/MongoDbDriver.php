<?php

namespace Wtf\Classes\Db;

use MongoDB\BSON\ObjectId;
use MongoDB\Collection;
use MongoDB\Database;
use MongoDB\DeleteResult;
use MongoDB\Driver\BulkWrite;
use MongoDB\Driver\Cursor;
use MongoDB\Driver\Exception\Exception;
use MongoDB\Driver\Manager;
use MongoDB\Driver\Query;
use MongoDB\InsertOneResult;
use MongoDB\UpdateResult;
use Traversable;

/**
 * Class MongoDbDriver
 * @package Wtf\Classes\Db
 */
class MongoDbDriver {
    /** @var Manager */
    protected $link = null;

    /** @var string */
    protected $database;

    /**
     * MongoDbDriver constructor.
     * @param Manager $link
     * @param string $database
     */
    public function __construct(Manager $link, string $database) {
        $this->link = $link;
        $this->database = $database;
    }

    public function query(string $sql) {
        die('not implemented');
    }

    /**
     * @param string $query
     * @param string $host
     * @param int $port
     * @param string $database
     * @param null|string $user
     * @param null|string $password
     * @param null|string $authDatabase
     * @return string
     */
    public static function veryRawQuery(
        string $query,
        string $host,
        int $port,
        string $database,
        ?string $user = null,
        ?string $password = null,
        ?string $authDatabase = null
    ): string {
        $credentials = '';
        if (!empty($user)) {
            $credentials = sprintf('-u "%s" -p "%s"', $user, $password);
            if (!empty($authDatabase)) {
                $credentials .= sprintf(' --authenticationDatabase "%s"', $authDatabase);
            }
        }
        $cmd = sprintf(
            "mongo --quiet %s:%d/%s  %s --eval '%s'",
            $host,
            $port,
            $database,
            $credentials,
            $query
        );

        return exec($cmd);
    }

    /**
     * @param string $collectionTitle
     * @param array $find
     * @param int|null $limit
     * @param array|null $sort
     * @param array $findOptions
     * @return array
     * @throws Exception
     */
    public function fetchDocuments(
        string $collectionTitle,
        array $find = [],
        int $limit = null,
        array $sort = null,
        array $findOptions = []
    ): array {
        if ($limit !== null) {
            $findOptions['limit'] = $limit;
        }
        if ($sort !== null) {
            $findOptions['sort'] = $sort;
        }

        $query = new Query($find, $findOptions);

        $cursor = $this->link->executeQuery($this->database.'.'.$collectionTitle, $query);
        return $cursor->toArray();
    }

    /**
     * @param string $collection
     * @param array $find
     * @param int|null $limit
     * @param array $options
     * @return Cursor
     * @throws Exception
     */
    public function fetchDocumentsCursor(
        string $collection,
        array $find = [],
        int $limit = null,
        array $options = []
    ): Cursor {
        if ($limit !== null) {
            $options['limit'] = $limit;
        }
        $query = new Query($find, $options);
        return $this->link->executeQuery($this->database.'.'.$collection, $query);
    }

    /**
     * @param string $collection
     * @param array $find
     * @param array|null $sort
     * @return array|null
     * @throws Exception
     */
    public function fetchDocument(string $collection, array $find = [], array $sort = null): ?array {
        $findOptions = [
            'limit' => 1,
        ];
        if ($sort !== null) {
            $findOptions['sort'] = $sort;
        }

        $query = new Query($find, $findOptions);
        $cursor = $this->link->executeQuery($this->database.'.'.$collection, $query);

        $rows = $cursor->toArray();
        if (isset($rows[0])) {
            return (array)$rows[0];
        }

        return null;
    }

    /**
     * @param string $collection
     * @param string $key
     * @param array $find
     * @param array $options
     * @return null|mixed
     * @throws Exception
     */
    public function fetchValue(string $collection, string $key, array $find = [], array $options = []) {
        $options['limit'] = 1;

        $query = new Query($find, $options);
        $cursor = $this->link->executeQuery($this->database.'.'.$collection, $query);

        foreach ($cursor as $item) {
            if (isset($item->{$key})) {
                return $item->{$key};
            }

            return null;
        }

        return null;
    }

    /**
     * @param string $collectionTitle
     * @param array $document
     * @param bool $upsert
     * @return InsertOneResult
     */
    public function insertOne(string $collectionTitle, array $document, bool $upsert = false): InsertOneResult {
        $collection = new Collection($this->link, $this->database, $collectionTitle);
        $options = [
            'upsert' => $upsert,
        ];
        return $collection->insertOne($document, $options);
    }

    /**
     * @param string $collection
     * @param array $documents
     * @return bool
     */
    public function insertIgnoreMany(string $collection, array $documents): bool {
        if (count($documents) === 0) {
            return false;
        }

        $bulk = new BulkWrite();
        foreach ($documents as $document) {
            $document['_id'] = new ObjectId();
            $bulk->insert($document);
        }

        try {
            $this->link->executeBulkWrite($this->database.'.'.$collection, $bulk);
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }

    /**
     * @param string $collection
     * @param array $documents
     * @param bool $debugTime
     * @return void
     */
    public function insertMany(string $collection, array $documents, bool $debugTime = false): void {
        if (count($documents) === 0) {
            return;
        }

        $start = microtime(true);

        $chunkSize = 50000;
        foreach (array_chunk($documents, $chunkSize) as $documentsChunk) {
            $bulk = new BulkWrite();
            foreach ($documentsChunk as $document) {
                $document['_id'] = new ObjectId();
                $bulk->insert($document);
            }

            $this->link->executeBulkWrite($this->database.'.'.$collection, $bulk);
        }

        if ($debugTime) {
            $timeElapsed = microtime(true) - $start;
            printf(
                'Written %d document(s) in %0.4f s. (%0.4f docs/s.)'.PHP_EOL,
                count($documents),
                $timeElapsed,
                count($documents) / $timeElapsed
            );
        }
    }

    /**
     * @param string $collection
     * @param array $documents
     * @param array $filterKeys
     * @param bool $debugTime
     * @return void
     */
    public function upsertMany(string $collection, array $documents, array $filterKeys, bool $debugTime = false): void {
        if (count($documents) === 0) {
            return;
        }

        $start = microtime(true);

        $chunkSize = 50000;
        foreach (array_chunk($documents, $chunkSize) as $documentsChunk) {
            $bulk = new BulkWrite();
            foreach ($documentsChunk as $document) {
                $filter = [];
                foreach ($filterKeys as $filterKey) {
                    $filter[$filterKey] = $document[$filterKey];
                }
                $bulk->update($filter, $document, ['upsert' => true]);
            }

            $this->link->executeBulkWrite($this->database.'.'.$collection, $bulk);
        }

        if ($debugTime) {
            $timeElapsed = microtime(true) - $start;
            printf(
                'Written %d document(s) in %0.4f s. (%0.4f docs/s.)'.PHP_EOL,
                count($documents),
                $timeElapsed,
                count($documents) / $timeElapsed
            );
        }
    }

    /**
     * @param string $collectionTitle
     * @param array $filter
     * @param array $update
     * @param bool $upsert
     * @return UpdateResult
     */
    public function updateOne(
        string $collectionTitle,
        array $filter,
        array $update,
        bool $upsert = false
    ): UpdateResult {
        $collection = new Collection($this->link, $this->database, $collectionTitle);
        $options = [
            'upsert' => $upsert,
        ];

        return $collection->updateOne($filter, $update, $options);
    }

    /**
     * @param string $collectionTitle
     * @param array $filter
     * @param array $update
     * @return UpdateResult
     */
    public function upsertOne(string $collectionTitle, array $filter, array $update): UpdateResult {
        return $this->updateOne($collectionTitle, $filter, $update, true);
    }

    /**
     * @param string $collectionTitle
     * @param array $filter
     * @param array $update
     * @param bool $upsert
     * @return UpdateResult
     */
    public function updateMany(
        string $collectionTitle,
        array $filter,
        array $update,
        bool $upsert = false
    ): UpdateResult {
        $collection = new Collection($this->link, $this->database, $collectionTitle);
        $options = [
            'upsert' => $upsert,
        ];

        return $collection->updateMany($filter, $update, $options);
    }

    /**
     * @param string $collectionTitle
     * @param array $find
     * @return DeleteResult
     */
    public function delete(string $collectionTitle, array $find = []): DeleteResult {
        $collection = new Collection($this->link, $this->database, $collectionTitle);
        return $collection->deleteMany($find);
    }

    /**
     * @param string $collectionTitle
     * @param array $filter
     * @param array $replacement
     * @param bool $upsert
     * @return UpdateResult
     */
    public function replaceOne(
        string $collectionTitle,
        array $filter,
        array $replacement,
        bool $upsert = true
    ): UpdateResult {
        $collection = new Collection($this->link, $this->database, $collectionTitle);
        $options = [
            'upsert' => $upsert,
        ];
        return $collection->replaceOne($filter, $replacement, $options);
    }

    /**
     *
     * @param string $table
     * @param array $rows
     * @return void
     */
    public function insertOnDuplicateUpdate(string $table, array $rows): void {
        die('insertOnDuplicateUpdate not implemented');
    }

    /**
     * @param string $value
     * @return string
     */
    public function escape(string $value): string {
        return addslashes($value);
    }

    /**
     * @param string $collectionTitle
     * @param string $key
     * @param array $filter
     * @return array
     */
    public function getDistinctValues(string $collectionTitle, string $key, array $filter = []): array {
        $collection = new Collection($this->link, $this->database, $collectionTitle);
        return (array)$collection->distinct($key, $filter);
    }

    /**
     * @param string $collectionTitle
     * @param array $filter
     * @param array $options
     * @return int|null
     */
    public function count(string $collectionTitle, array $filter, array $options = []): ?int {
        $command = [
            'count' => $collectionTitle,
        ];

        $command['query'] = $filter;

        if (!empty($options['limit'])) {
            $command['limit'] = $options['limit'];
        }

        if (!empty($options['skip'])) {
            $command['skip'] = $options['skip'];
        }

        if (!empty($options['hint'])) {
            $command['hint'] = $options['hint'];
        }

        $commandResult = (new Database($this->link, $this->database))->command($command)
            ->toArray();

        if (empty($commandResult) || empty($commandResult[0]->ok)) {
            return null;
        }

        return (int)$commandResult[0]->n;
    }

    /**
     * @param string $collectionTitle
     * @param array $pipeline
     * @param array $options
     * @return Traversable|Cursor
     */
    public function aggregate(string $collectionTitle, array $pipeline, array $options = []) {
        $collection = new Collection($this->link, $this->database, $collectionTitle);
        return $collection->aggregate($pipeline, $options);
    }

    /**
     * Alias of beginTransaction()
     */
    public function startTransaction() {
        die('startTransaction not implemented');
    }

    public function beginTransaction() {
        die('beginTransaction not implemented');
    }

    public function beginTransactionReadOnly() {
        die('beginTransactionReadOnly not implemented');
    }

    public function beginTransactionReadWrite() {
        die('beginTransactionReadWrite not implemented');
    }

    public function beginTransactionWithConsistentSnapshot() {
        die('beginTransactionWithConsistentSnapshot not implemented');
    }

    public function commit() {
        die('commit not implemented');
    }

    public function rollback() {
        die('rollback not implemented');
    }

    public function setAutoCommit(bool $mode) {
        die('setAutoCommit not implemented');
    }

    /**
     * @param BulkWrite $bulk
     * @param string $collectionTitle
     */
    public function executeBulkWrite(BulkWrite $bulk, string $collectionTitle) {
        $this->link->executeBulkWrite($this->database.'.'.$collectionTitle, $bulk);
    }

    /**
     * @param string $name
     * @param array $options
     * @return Collection
     */
    public function collection(string $name, array $options = []): Collection {
        return new Collection($this->link, $this->database, $name, $options);
    }
}
