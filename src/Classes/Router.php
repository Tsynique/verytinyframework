<?php

namespace Wtf\Classes;

use ReflectionMethod;
use Wtf\Helpers\Config;
use Wtf\Wtf;

class Router {
    protected static $routes = null;

    public function initRoutesFromDotRoutes() {
        return $this->initRoutesFromConfig(Wtf::ROUTES_FILE_PATH);
    }

    public function initRoutesFromConfig(string $routesFilePath) {
        if (self::$routes !== null) {
            return;
        }

        $config = new Config($routesFilePath);
        if (!$config->exists()) {
            self::$routes = [];
            return;
        }

        self::$routes = $config->readValues();
    }

    public function getResolvedRoute(string $route): ?array {
        if (empty(self::$routes)) {
            return null;
        }

        foreach (self::$routes as $regex => $call) {
            $matches = [];
            if (preg_match('/'.$regex.'/', $route, $matches) === 0) {
                continue;
            }

            $params = array_slice($matches, 1);

            $routeCall = $this->getResolvedRouteCall($call);
            if ($routeCall === null) {
                return null;
            }

            return [
                'call' => $routeCall,
                'params' => $params,
            ];
        }

        return null;
    }

    public function getResolvedRouteCall(string $call): ?array {
        $classNameAndMethod = explode('::', $call);
        if (count($classNameAndMethod) !== 2) {
            return null;
        }

        $method = str_replace(['(', ')'], '', $classNameAndMethod[1]);
        return [
            'class' => $classNameAndMethod[0],
            'method' => $method,
        ];
    }

    public function callByRoute(string $route): bool {
        $resolved = $this->getResolvedRoute($route);
        if ($resolved === null) {
            return false;
        }

        $params = $resolved['params'];

        $method = new ReflectionMethod($resolved['call']['class'], $resolved['call']['method']);
        if ($method->isStatic()) {
            $callback = [
                $resolved['call']['class'],
                $resolved['call']['method'],
            ];
            call_user_func_array($callback, $params);
        } else {
            $className = $resolved['call']['class'];
            $class = new $className();
            call_user_func_array([$class, $resolved['call']['method']], $params);
        }
        
        return true;
    }
}
